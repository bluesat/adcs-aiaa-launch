EESchema Schematic File Version 2
LIBS:MPU-92:65
LIBS:sd_card
LIBS:arduinopromini
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos_ieee
LIBS:cmos4000
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intel
LIBS:interface
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:linear
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:memory
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:microchip
LIBS:microcontrollers
LIBS:modules
LIBS:motor_drivers
LIBS:motorola
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:nxp
LIBS:onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:Power_Management
LIBS:power
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:zetex
LIBS:Zilog
LIBS:ds3231m
LIBS:PCB TEMP-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ADCS Subsystem for AIAA Launch"
Date "24.3.2019"
Rev "1"
Comp "BLUEsat UNSW"
Comment1 "ADCS Subsystem for the April 17 AIAA Launch"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole MH1
U 1 1 5C70847C
P 1800 7200
F 0 "MH1" H 1900 7246 50  0000 L CNN
F 1 "MountingHole" H 1900 7155 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 1800 7200 50  0001 C CNN
F 3 "~" H 1800 7200 50  0001 C CNN
	1    1800 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH3
U 1 1 5C70850E
P 3250 7200
F 0 "MH3" H 3350 7246 50  0000 L CNN
F 1 "MountingHole" H 3350 7155 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 3250 7200 50  0001 C CNN
F 3 "~" H 3250 7200 50  0001 C CNN
	1    3250 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH4
U 1 1 5C70857C
P 3950 7200
F 0 "MH4" H 4050 7246 50  0000 L CNN
F 1 "MountingHole" H 4050 7155 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 3950 7200 50  0001 C CNN
F 3 "~" H 3950 7200 50  0001 C CNN
	1    3950 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH2
U 1 1 5C7085DC
P 2550 7200
F 0 "MH2" H 2650 7246 50  0000 L CNN
F 1 "MountingHole" H 2650 7155 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 2550 7200 50  0001 C CNN
F 3 "~" H 2550 7200 50  0001 C CNN
	1    2550 7200
	1    0    0    -1  
$EndComp
NoConn ~ 8450 3300
NoConn ~ 8450 3200
NoConn ~ 8450 3150
NoConn ~ 8450 3100
NoConn ~ 8450 3000
NoConn ~ 8450 2950
NoConn ~ 8450 2900
NoConn ~ 8450 2350
NoConn ~ 8450 2300
NoConn ~ 8450 2200
NoConn ~ 8450 2100
NoConn ~ 8450 2050
NoConn ~ 8200 3550
NoConn ~ 8150 3550
NoConn ~ 7850 3550
NoConn ~ 7800 3550
NoConn ~ 7750 3550
NoConn ~ 7400 1800
NoConn ~ 7500 1800
NoConn ~ 7650 1800
NoConn ~ 7700 1800
NoConn ~ 8000 1800
NoConn ~ 8100 1800
NoConn ~ 8200 1800
$Comp
L power:GND #PWR01
U 1 1 5C7BAF9B
P 6750 1750
F 0 "#PWR01" H 6750 1500 50  0001 C CNN
F 1 "GND" H 6755 1577 50  0000 C CNN
F 2 "" H 6750 1750 50  0001 C CNN
F 3 "" H 6750 1750 50  0001 C CNN
	1    6750 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5C7BC2F5
P 8650 3750
F 0 "#PWR02" H 8650 3500 50  0001 C CNN
F 1 "GND" H 8655 3577 50  0000 C CNN
F 2 "" H 8650 3750 50  0001 C CNN
F 3 "" H 8650 3750 50  0001 C CNN
	1    8650 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5C7BFE55
P 6500 3300
F 0 "#PWR03" H 6500 3050 50  0001 C CNN
F 1 "GND" H 6505 3127 50  0000 C CNN
F 2 "" H 6500 3300 50  0001 C CNN
F 3 "" H 6500 3300 50  0001 C CNN
	1    6500 3300
	1    0    0    -1  
$EndComp
Text Label 7300 3750 0    30   ~ 0
5V
Text Label 6650 2600 2    50   ~ 0
5V
Text Label 6650 2250 2    50   ~ 0
3V3
Text Label 6700 3750 2    30   ~ 0
3V3
Text Label 7000 1800 1    30   ~ 0
5V
Text Label 8450 2400 0    30   ~ 0
3V3
Text Label 8450 2250 0    30   ~ 0
5V
Text Label 7650 3550 3    30   ~ 0
3V3
Text Label 7450 1800 1    30   ~ 0
5V
Text Label 8050 1800 1    30   ~ 0
5V
Text Label 7900 1800 1    30   ~ 0
3V3
Text Label 7600 1800 1    30   ~ 0
3V3
Text Label 7050 1800 1    30   ~ 0
OBC1
Text Label 7100 1800 1    30   ~ 0
OBC2
Text Label 7200 1800 1    30   ~ 0
OBC3
Text Label 7250 1800 1    30   ~ 0
OBC4
Text Label 7300 1800 1    30   ~ 0
OBC5
Text Label 8450 2500 0    30   ~ 0
OBC6
Text Label 8450 2550 0    30   ~ 0
OBC7
Text Label 8450 2600 0    30   ~ 0
OBC8
Text Label 8450 2700 0    30   ~ 0
POW1
Text Label 8450 2750 0    30   ~ 0
POW2
Text Label 8450 2800 0    30   ~ 0
POW3
Text Label 7950 3550 3    30   ~ 0
POW6
Text Label 8000 3550 3    30   ~ 0
POW5
Text Label 8050 3550 3    30   ~ 0
POW4
$Comp
L MPU-92/65 U3
U 1 1 5C80AE94
P 7450 4600
F 0 "U3" H 7450 5150 50  0000 C CNN
F 1 "MPU-92/65" H 7500 3550 50  0000 C CNN
F 2 "MPU-92:MPU-92.65" H 9350 4250 50  0001 C CNN
F 3 "" H 9350 4250 50  0001 C CNN
	1    7450 4600
	1    0    0    -1  
$EndComp
$Comp
L ArduinoProMini U2
U 1 1 5C80AEE1
P 4200 4500
F 0 "U2" H 4200 3450 60  0000 C CNN
F 1 "ArduinoProMini" H 4200 5250 60  0000 C CNN
F 2 "ArduinoMiniPro:ArduinoProMicro" H 4200 4500 60  0001 C CNN
F 3 "" H 4200 4500 60  0001 C CNN
	1    4200 4500
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 5C80BA9C
P 4850 1800
F 0 "R2" V 4930 1800 50  0000 C CNN
F 1 "10K" V 4850 1800 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4780 1800 50  0001 C CNN
F 3 "" H 4850 1800 50  0001 C CNN
	1    4850 1800
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5C80BAE8
P 4600 1800
F 0 "R1" V 4680 1800 50  0000 C CNN
F 1 "10K" V 4600 1800 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4530 1800 50  0001 C CNN
F 3 "" H 4600 1800 50  0001 C CNN
	1    4600 1800
	1    0    0    -1  
$EndComp
$Comp
L SOLAR:PCbus U1
U 1 1 5C713480
P 6900 2000
F 0 "U1" H 7941 1671 50  0000 L CNN
F 1 "PCbus" H 7941 1580 50  0000 L CNN
F 2 "PC BUS:BUS PC104" H 6900 2000 50  0001 C CNN
F 3 "" H 6900 2000 50  0001 C CNN
	1    6900 2000
	1    0    0    -1  
$EndComp
$Comp
L SD_card U4
U 1 1 5C80CA48
P 6000 5250
F 0 "U4" H 6000 4300 50  0000 C CNN
F 1 "SD_card" H 6000 5650 50  0000 C CNN
F 2 "SD_Card:SD_card" H 6000 4300 50  0001 C CNN
F 3 "" H 6000 4300 50  0001 C CNN
	1    6000 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 2150 8650 2150
Wire Wire Line
	6950 1800 6950 1600
Wire Wire Line
	8150 1600 8150 1800
Wire Wire Line
	7950 1800 7950 1600
Connection ~ 7950 1600
Wire Wire Line
	3450 1600 8150 1600
Wire Wire Line
	7750 1800 7750 1600
Connection ~ 7750 1600
Wire Wire Line
	7550 1800 7550 1600
Connection ~ 7550 1600
Wire Wire Line
	7350 1800 7350 1600
Connection ~ 7350 1600
Wire Wire Line
	7150 1800 7150 1600
Connection ~ 7150 1600
Wire Wire Line
	6750 1600 6750 1750
Connection ~ 6950 1600
Wire Wire Line
	8450 2450 8650 2450
Wire Wire Line
	8650 2150 8650 3750
Wire Wire Line
	8650 3250 8450 3250
Connection ~ 8650 2450
Wire Wire Line
	8650 2650 8450 2650
Connection ~ 8650 2650
Wire Wire Line
	8650 2850 8450 2850
Connection ~ 8650 2850
Wire Wire Line
	8650 3050 8450 3050
Connection ~ 8650 3050
Wire Wire Line
	7700 3550 7700 3750
Wire Wire Line
	8650 3750 7550 3750
Connection ~ 8650 3250
Wire Wire Line
	7900 3750 7900 3550
Connection ~ 7900 3750
Wire Wire Line
	8100 3750 8100 3550
Connection ~ 8100 3750
Connection ~ 8650 3750
Wire Wire Line
	7550 3750 7550 3550
Connection ~ 7700 3750
Wire Wire Line
	7600 3550 7600 3750
Connection ~ 7600 3750
Wire Wire Line
	6700 2050 6500 2050
Wire Wire Line
	6500 2050 6500 3300
Wire Wire Line
	6500 3100 6700 3100
Connection ~ 6500 3100
Wire Wire Line
	6500 3050 6700 3050
Connection ~ 6500 3050
Wire Wire Line
	6500 3000 6700 3000
Connection ~ 6500 3000
Wire Wire Line
	6500 2950 6700 2950
Connection ~ 6500 2950
Wire Wire Line
	6500 2100 6700 2100
Connection ~ 6500 2100
Wire Wire Line
	7050 3550 7500 3550
Connection ~ 7100 3550
Connection ~ 7150 3550
Connection ~ 7200 3550
Connection ~ 7250 3550
Connection ~ 7300 3550
Connection ~ 7350 3550
Connection ~ 7400 3550
Connection ~ 7450 3550
Wire Wire Line
	7300 3550 7300 3750
Wire Wire Line
	6700 2450 6700 2900
Connection ~ 6700 2500
Connection ~ 6700 2550
Connection ~ 6700 2600
Connection ~ 6700 2650
Connection ~ 6700 2700
Connection ~ 6700 2750
Connection ~ 6700 2800
Connection ~ 6700 2850
Wire Wire Line
	6700 2600 6650 2600
Wire Wire Line
	6700 3150 6700 3750
Wire Wire Line
	6700 3550 7000 3550
Connection ~ 6700 3200
Connection ~ 6700 3250
Connection ~ 6700 3300
Connection ~ 6950 3550
Connection ~ 6700 3550
Wire Wire Line
	6700 2150 6700 2400
Connection ~ 6700 2200
Connection ~ 6700 2250
Connection ~ 6700 2300
Connection ~ 6700 2350
Wire Wire Line
	6700 2250 6650 2250
Wire Wire Line
	4150 1500 4150 2600
Connection ~ 6750 1600
Wire Wire Line
	5150 1700 5150 3050
Wire Wire Line
	5150 3050 4850 3050
Wire Wire Line
	5350 1800 5350 3200
Wire Wire Line
	5350 3200 4850 3200
Wire Wire Line
	4850 1650 4850 1500
Connection ~ 4850 1500
Wire Wire Line
	4600 1500 4600 1650
Connection ~ 4600 1500
Wire Wire Line
	4850 1950 5150 1950
Connection ~ 5150 1950
Wire Wire Line
	4600 1950 4600 2100
Wire Wire Line
	4600 2100 5350 2100
Connection ~ 5350 2100
Wire Wire Line
	6950 4150 6250 4150
Wire Wire Line
	6250 4150 6250 1500
Connection ~ 6250 1500
Wire Wire Line
	6950 4300 6100 4300
Wire Wire Line
	6100 4300 6100 1600
Connection ~ 6100 1600
Wire Wire Line
	6950 4450 6000 4450
Wire Wire Line
	6000 4450 6000 1700
Connection ~ 6000 1700
Wire Wire Line
	6950 4600 5900 4600
Wire Wire Line
	5900 4600 5900 1800
Connection ~ 5900 1800
Wire Wire Line
	5500 5000 5500 1600
Connection ~ 5500 1600
Wire Wire Line
	4850 3800 5500 3800
Connection ~ 5500 3800
Wire Wire Line
	5500 6050 5100 6050
Wire Wire Line
	5100 6050 5100 3800
Connection ~ 5100 3800
Wire Wire Line
	5500 5300 5300 5300
Wire Wire Line
	5300 5300 5300 4100
Wire Wire Line
	5300 4100 4850 4100
Wire Wire Line
	5250 4100 5250 1500
Connection ~ 5250 1500
Connection ~ 5250 4100
Wire Wire Line
	3450 1600 3450 3650
Wire Wire Line
	3450 3650 3600 3650
Connection ~ 4050 1600
Wire Wire Line
	5500 5450 4850 5450
Wire Wire Line
	4850 5450 4850 5300
Wire Wire Line
	4850 5150 4950 5150
Wire Wire Line
	4950 5150 4950 5600
Wire Wire Line
	4950 5600 5500 5600
Wire Wire Line
	4850 5000 5150 5000
Wire Wire Line
	5150 5000 5150 5900
Wire Wire Line
	5150 5900 5500 5900
Wire Wire Line
	4850 4850 5200 4850
Wire Wire Line
	5200 4850 5200 5750
Wire Wire Line
	5200 5750 5500 5750
Wire Wire Line
	4150 1500 6400 1500
Wire Wire Line
	4050 2600 4050 1600
Wire Wire Line
	6400 1500 6400 1200
Wire Wire Line
	7000 1200 7000 1800
Wire Wire Line
	6400 1200 7000 1200
Text Label 7800 1800 1    30   ~ 0
DASW1
Wire Wire Line
	3600 3850 3300 3850
Wire Wire Line
	3300 3850 3300 1000
Wire Wire Line
	3300 1000 7800 1000
Wire Wire Line
	7800 1000 7800 1800
Text Label 7850 1800 1    30   ~ 0
DASW2
Wire Wire Line
	7850 1800 7850 900 
Wire Wire Line
	7850 900  3200 900 
Wire Wire Line
	3200 900  3200 4050
Wire Wire Line
	3200 4050 3600 4050
Wire Wire Line
	5150 1700 7100 1700
Wire Wire Line
	7100 1700 7100 1800
Wire Wire Line
	5350 1800 7200 1800
$EndSCHEMATC
